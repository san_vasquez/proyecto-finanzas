﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.Filters;
using PROYECTO_FINANZAS.Web.Helpers;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class DashboardController : BaseController
    {
        [RolAuthFilter()]
        public ActionResult Index()
        {
            return View();
        }
    }
}