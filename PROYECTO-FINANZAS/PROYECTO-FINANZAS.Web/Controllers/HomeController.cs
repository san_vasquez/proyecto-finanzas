﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using PROYECTO_FINANZAS.Web.ViewModel.Home;
using System.Transactions;
using PROYECTO_FINANZAS.Web.Models;

namespace PROYECTO_FINANZAS.Web.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(String NombreUsuario, String Password)
        {
            var ObjUsuario = Context.Usuario.FirstOrDefault(X => X.NombreUsuario == NombreUsuario && X.Password == Password);

            if (ObjUsuario != null)
            {
                if (ObjUsuario.Bloqueado)
                {
                    TempData["Mensaje"] = "Problema de autorización: El usuario se encuentra bloqueado";
                    return View();
                }

                Session["UsuarioId"] = ObjUsuario.UsuarioId;
                Session["NombreUsuario"] = ObjUsuario.NombreUsuario;
                Session["NombrePerfil"] = ObjUsuario.Perfil.FirstOrDefault().Nombre;
                Session["PerfilId"] = ObjUsuario.Perfil.FirstOrDefault().UsuarioId;
                return RedirectToAction("Index", "Dashboard");
            }
            else
            {
                TempData["Mensaje"] = "Problema de autentificación: Usuario o Password incorrectos";
                return View();
            }
        }

        public ActionResult Logout()
        {
            Session.Clear();
            return RedirectToAction("Login");
        }

        public ActionResult Registro()
        {
            RegistroViewModel ViewModel = new RegistroViewModel();
            return View(ViewModel);
        }

        [HttpPost]
        public ActionResult Registro(RegistroViewModel ViewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    TryUpdateModel(ViewModel);
                    TempData["Mensaje"] = "Por favor, complete los campos requeridos";
                    return View(ViewModel);
                }

                using (var tc = new TransactionScope())
                {
                    Usuario ObjUsuario = new Usuario();
                    ObjUsuario.NombreUsuario = ViewModel.Username;
                    ObjUsuario.Password = ViewModel.Password;
                    ObjUsuario.Bloqueado = false;
                    Context.Usuario.Add(ObjUsuario);
                    
                    Perfil ObjPerfil = new Perfil();
                    ObjPerfil.Nombre = ViewModel.Nombre.ToUpper();
                    ObjPerfil.Apellido = ViewModel.Apellido.ToUpper();
                    ObjPerfil.Email = ViewModel.Email;
                    ObjPerfil.Dni = ViewModel.Dni;
                    ObjPerfil.UsuarioId = ObjUsuario.UsuarioId;
                    Context.Perfil.Add(ObjPerfil);

                    Context.SaveChanges();
                    TempData["Mensaje"] = "Su cuenta se registro exitosamente";
                    tc.Complete();
                }
            }
            catch (Exception ex)
            {
                TryUpdateModel(ViewModel);
                TempData["Mensaje"] = "Error durante el registro, vuelva a intentarlo.";
                return View(ViewModel);
            }
            return RedirectToAction("Login");
        }
    }
}
