﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PROYECTO_FINANZAS.Web.Startup))]
namespace PROYECTO_FINANZAS.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
