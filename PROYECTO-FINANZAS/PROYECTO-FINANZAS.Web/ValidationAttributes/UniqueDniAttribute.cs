﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using PROYECTO_FINANZAS.Web.Util;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class UniqueDniAttribute : ValidationAttribute
    {
        public UniqueDniAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Dni = value.ToString();

                if (Dni.Length != 8)
                    return false;

                if (!Dni.All(X => Char.IsNumber(X)))
                    return false;

                return !WebUtil.esDniRepetido(Dni);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
