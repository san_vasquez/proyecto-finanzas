﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

using PROYECTO_FINANZAS.Web.Util;

namespace PROYECTO_FINANZAS.Web.ValidationAttributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = false)]
    public class UniqueUsernameAttribute : ValidationAttribute
    {
        public UniqueUsernameAttribute() { }

        public override bool IsValid(object value)
        {
            try
            {
                String Username = value.ToString();
                return !WebUtil.esUserNameRepetido(Username);
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}