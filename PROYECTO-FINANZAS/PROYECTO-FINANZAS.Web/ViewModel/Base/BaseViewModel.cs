﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.Models;

namespace PROYECTO_FINANZAS.Web.ViewModel.Base
{
    public class BaseViewModel
    {
        protected FinanzasEntities Context;

        public BaseViewModel()
        {
            Context = new FinanzasEntities();
        }
    }
}