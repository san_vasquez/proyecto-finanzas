﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.ComponentModel.DataAnnotations;
using PROYECTO_FINANZAS.Web.ValidationAttributes;

namespace PROYECTO_FINANZAS.Web.ViewModel.Home
{
    public class RegistroViewModel
    {
        [Required]
        [UniqueUsername]
        public String Username { get; set; }

        [Required]
        public String Nombre { get; set; }

        [Required]
        public String Apellido { get; set; }

        [Required]
        [UniqueDni]
        public String Dni { get; set; }

        [Required]
        [EmailAddress]
        [UniqueEmail]
        public String Email { get; set; }

        [Required]
        public String Password { get; set; }

        public RegistroViewModel() { }
    }
}
