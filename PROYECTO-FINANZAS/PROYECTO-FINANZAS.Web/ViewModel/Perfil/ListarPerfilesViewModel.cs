﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using PROYECTO_FINANZAS.Web.ViewModel.Base;

namespace PROYECTO_FINANZAS.Web.ViewModel.Perfil
{
    public class ListarPerfilesViewModel : BaseViewModel
    {
        public List<Models.Perfil> LstPerfiles;

        public ListarPerfilesViewModel() { }

        public void CargarDatos(int UsuarioId)
        {
            LstPerfiles = Context.Perfil.OrderByDescending(X => X.Nombre).Where(X => X.UsuarioId == UsuarioId).ToList();
        }
    }
}
